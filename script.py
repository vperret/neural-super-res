import neural_image_processor as nip

config_file = 'config/EES.config'
data = nip.prepare_training_data(config_file)
nip.train(config_file,*data)
nip.predict(config_file) 
nip.psnr(config_file) 
