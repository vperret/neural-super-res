#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import cv2
import h5py
import json
import math
import keras
import numpy
import pandas
import operator
import configparser
import matplotlib.pyplot as plt
from keras import backend as K
from tqdm import tqdm
import matplotlib.pyplot as plt
import tensorflow as tf
from skimage.util import view_as_blocks
import skimage.transform
import skimage.measure
import skimage.filters
import skimage.io

def divisors(n):
    divs = []
    for i in range(2,int(math.sqrt(n))+1):
        if n%i == 0:
            divs.extend([i,n/i])
    divs.extend([n])
    return numpy.sort(numpy.array(list(set(divs))).astype(int))


#numpy.random.seed(0)
class config_obj():
    """ Object containing the configuration of the neural network """

    def parse(self, config_file):
        """ Configuration file parser """
        config = configparser.SafeConfigParser()
        config.read(config_file)
        try:
            self.random_crops = config.getint('train', 'random_crops')
        except:
            self.random_crops = 32
        try:
            self.patch_size = config.getint('train', 'patch_size')
        except:
            self.patch_size = 32
        try:
            self.upscale = config.getint('train', 'upscale')
        except:
            self.upscale = 2
        try:
            self.optimizer = config.get('train', 'optimizer')
        except:
            self.optimizer = 'adam'
        try:
            self.batch_size = config.getint('train', 'batch_size')
        except:
            self.batch_size = 64
        try:
            self.nfilters = config.getint('train', 'nfilters')
        except:
            self.nfilters = 16
        try:
            self.nepochs = json.loads(config.get('train', 'nepochs'))
        except:
            self.nepochs = [100]
        try:
            self.blur = config.getfloat('train', 'blur')
        except:
            self.blur = 0.0
        try:
            self.learn_rates = json.loads(config.get('train', 'learn_rates'))
        except:
            self.learn_rates = [1e-3]
        try:
            self.dropout = config.getfloat('train', 'dropout')
        except:
            self.dropout = 0.20
        try:
            self.model_type = config.get('train', 'model_type')
        except:
            self.model_type = 'EEDS'
        try:
            self.plot = config.getboolean('train', 'plot')
        except:
            self.plot = False
        try:
            self.train_data = config.get('train', 'train_data')
        except:
            self.train_data = 'pixel_sample/train'
        try:
            self.jpeg_noise = config.getint('train', 'jpeg_noise')
        except:
            self.jpeg_noise = 100
        try:
            self.input_image = config.get('predict', 'input_image')
        except:
            self.input_image = 'input.jpg'
        try:
            self.process_size = config.getint('predict', 'process_size')
        except:
            self.process_size = 512

        self.label_size = self.patch_size*self.upscale

def prepare_training_data(config_file):

    config = config_obj()
    config.parse(config_file)

    train_filename = "data/train_up"+str(config.upscale)+"_jnoise"+str(config.jpeg_noise)+"_p"+str(config.patch_size)+"_ncrops"+str(config.random_crops)+"_blur"+str(config.blur)
    if(os.path.isfile(train_filename+".h5")):
        data_train, label_train = load_hdf5_data(train_filename+".h5")
    else:
        print("Loading training dataset")
        names = os.listdir(config.train_data)
        names = sorted(names)
        nums = names.__len__()
        if(config.plot):
            fig,ax = plt.subplots(nums,config.random_crops)
        data_train = numpy.zeros((nums * config.random_crops, config.patch_size, config.patch_size, 1), dtype=numpy.double)
        label_train = numpy.zeros((nums * config.random_crops, config.label_size, config.label_size, 1), dtype=numpy.double)
        for i in range(nums):
            print(names[i])
            name = config.train_data +'/'+ names[i]
            hr_img = skimage.io.imread(name)
            hr_img = hr_img/255.
            hr_blur = skimage.filters.gaussian(hr_img,config.blur,multichannel=True)
            if(config.jpeg_noise<100):
                hr_blur = skimage.io.imsave('temp.jpg',hr_blur,quality=config.jpeg_noise)
                hr_blur = skimage.io.imread('temp.jpg')
                hr_blur = hr_blur
                os.remove('temp.jpg')
            shape = hr_img.shape

            # produce Random_Crop random coordinate to crop training img
            if(min(shape[0], shape[1]) - config.label_size < 0):
                continue
            Points_x = numpy.random.randint(0, min(shape[0], shape[1]) - config.label_size, config.random_crops)
            Points_y = numpy.random.randint(0, min(shape[0], shape[1]) - config.label_size, config.random_crops)

            for j in range(config.random_crops):
                id = int(numpy.random.rand(1)*3.0)
                hr_patch = hr_img[Points_x[j]: Points_x[j] + config.label_size, Points_y[j]: Points_y[j] + config.label_size, id]
                hrb_patch = hr_blur[Points_x[j]: Points_x[j] + config.label_size, Points_y[j]: Points_y[j] + config.label_size, id]
                lr_patch = skimage.transform.resize(hrb_patch, (int(config.label_size / config.upscale), int(config.label_size / config.upscale)), mode='constant')
                if(config.plot):
                    ax[i,j].imshow(hr_patch,interpolation='nearest')
                    ax[i,j].set_xticks([])
                    ax[i,j].set_yticks([])
                lr_patch = lr_patch.astype(float)
                hr_patch = hr_patch.astype(float)

                data_train[i * config.random_crops + j, :, :, :] = lr_patch[:,:,numpy.newaxis]
                label_train[i * config.random_crops + j, :, :, :] = hr_patch[:,:,numpy.newaxis]

        write_hdf5(data_train, label_train, train_filename+".h5")
        if(config.plot):
            plt.tight_layout()
            plt.savefig(train_filename+".pdf")
            plt.close()

    return data_train, label_train


def write_hdf5(data, labels, output_filename):
    """
    This function is used to save image data and its label(s) to hdf5 file.
    output_file.h5,contain data and label
    """

    x = data.astype(numpy.float32)
    y = labels.astype(numpy.float32)

    with h5py.File(output_filename, 'w') as h:
        h.create_dataset('data', data=x, shape=x.shape)
        h.create_dataset('label', data=y, shape=y.shape)
        # h.create_dataset()

def load_hdf5_data(file):
    with h5py.File(file, 'r') as hf:
        data = numpy.array(hf.get('data'))
        label = numpy.array(hf.get('label'))
        return data, label

def Res_block():
    _input = keras.layers.Input(shape=(None, None, 64))
    conv1 = keras.layers.Conv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(_input)
    conv2 = keras.layers.Conv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(conv1)
    out = keras.layers.add(inputs=[conv1, conv2])
    model = keras.models.Model(inputs=_input, outputs=out)

    return model

def model_EES(config):
    _input = keras.layers.Input(shape=(None, None, 1), name='input')

    EES = keras.layers.Conv2D(filters=8, kernel_size=(3, 3), strides=(1, 1), kernel_initializer='he_normal', padding='same', activation='relu')(_input)
    EES = keras.layers.Conv2DTranspose(filters=16, kernel_size=(14, 14), strides=(config.upscale, config.upscale),padding='same', activation='relu')(EES)
    out = keras.layers.Conv2D(filters=1, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='relu')(EES)

    model = keras.models.Model(inputs=_input, outputs=out)
    return model

def model_EES16(config):
    _input = keras.layers.Input(shape=(None, None, 1), name='input')

    EES = keras.layers.Conv2D(filters=16, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(_input)
    EES = keras.layers.Conv2DTranspose(filters=16, kernel_size=(config.upscale*3, config.upscale*3), strides=(config.upscale, config.upscale),padding='same', activation='relu')(EES)
    EES = keras.layers.Conv2D(filters=16, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(EES)
    out = keras.layers.Conv2D(filters=1, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(EES)

    model = keras.models.Model(inputs=_input, outputs=out)
    return model

def model_EES32(config):
    _input = keras.layers.Input(shape=(None, None, 1), name='input')

    EES = keras.layers.Conv2D(filters=32, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(_input)
    EES = keras.layers.Conv2DTranspose(filters=32, kernel_size=(config.upscale*3, config.upscale*3), strides=(config.upscale, config.upscale),padding='same', activation='relu')(EES)
    EES = keras.layers.Conv2D(filters=32, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(EES)
    out = keras.layers.Conv2D(filters=1, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(EES)

    model = keras.models.Model(inputs=_input, outputs=out)
    return model

def model_EES64(config):
    _input = keras.layers.Input(shape=(None, None, 1), name='input')

    EES = keras.layers.Conv2D(filters=64, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='relu')(_input)
    EES = keras.layers.Conv2DTranspose(filters=64, kernel_size=(config.upscale*5, config.upscale*5), strides=(config.upscale, config.upscale),padding='same', activation='relu')(EES)
    EES = keras.layers.Conv2D(filters=64, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='relu')(EES)
    out = keras.layers.Conv2D(filters=1, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='relu')(EES)

    model = keras.models.Model(inputs=_input, outputs=out)
    return model


def model_EED(config):
    _input = keras.layers.Input(shape=(None, None, 1), name='input')

    Feature1 = keras.layers.Conv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(_input)
    Feature1 = keras.layers.Conv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(Feature1)
    Feature2 = keras.layers.Conv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(Feature1)
    Feature_out = keras.layers.add(inputs=[Feature1,Feature2])

    # Upsampling
    Upsampling1 = keras.layers.Conv2D(filters=8, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='relu')(Feature_out)
    Upsampling2 = keras.layers.Conv2DTranspose(filters=16, kernel_size=(14, 14), strides=(config.upscale, config.upscale),padding='same', activation='relu')(Upsampling1)
    Upsampling3 = keras.layers.Conv2D(filters=64, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='relu')(Upsampling2)

    # Multi-scale Reconstruction
    Block1 = Res_block()(Upsampling3)
    Block2 = Res_block()(Block1)

    # ***************//
    Multi_scale1 = keras.layers.Conv2D(filters=16, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='relu')(Block2)

    Multi_scale2a = keras.layers.Conv2D(filters=16, kernel_size=(1, 1), strides=(1, 1),
                           padding='same', activation='relu')(Multi_scale1)

    Multi_scale2b = keras.layers.Conv2D(filters=16, kernel_size=(1, 3), strides=(1, 1),
                           padding='same', activation='relu')(Multi_scale1)
    Multi_scale2b = keras.layers.Conv2D(filters=16, kernel_size=(3, 1), strides=(1, 1),
                           padding='same', activation='relu')(Multi_scale2b)

    Multi_scale2c = keras.layers.Conv2D(filters=16, kernel_size=(1, 5), strides=(1, 1),
                           padding='same', activation='relu')(Multi_scale1)
    Multi_scale2c = keras.layers.Conv2D(filters=16, kernel_size=(5, 1), strides=(1, 1),
                           padding='same', activation='relu')(Multi_scale2c)

    Multi_scale2d = keras.layers.Conv2D(filters=16, kernel_size=(1, 7), strides=(1, 1),
                           padding='same', activation='relu')(Multi_scale1)
    Multi_scale2d = keras.layers.Conv2D(filters=16, kernel_size=(7, 1), strides=(1, 1),
                           padding='same', activation='relu')(Multi_scale2d)

    Multi_scale2 = keras.layers.concatenate(inputs=[Multi_scale2a, Multi_scale2b, Multi_scale2c, Multi_scale2d])

    out = keras.layers.Conv2D(filters=1, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='relu')(Multi_scale2)
    model = keras.models.Model(inputs=_input, outputs=out)
    model.summary()

    return model

def model_EEDS(config):
    _input = keras.layers.Input(shape=(None, None, 1), name='input')

    EES = model_EES(config)(_input)
    EED = model_EED(config)(_input)
    _EEDS = keras.layers.add(inputs=[EED, EES])

    model = keras.models.Model(inputs=_input, outputs=_EEDS)
    model.summary()
    return model


def train(config_file,data_train,label_train):

    config = config_obj()
    config.parse(config_file)

    myopt = operator.attrgetter(config.optimizer)(keras.optimizers)
    possibles = globals().copy()
    possibles.update(locals())
    mymodel = possibles.get("model_"+config.model_type)
    model = mymodel(config)
    model.compile(optimizer=myopt(), loss='mse')
    model.summary()

    checkpoint1 = keras.callbacks.ModelCheckpoint("models/"+config.model_type+"_up"+str(config.upscale)+"_jnoise"+str(config.jpeg_noise)+"_blur"+str(config.blur)+"_check_val_loss.h5", monitor='val_loss', verbose=0, save_best_only=True,
                                 save_weights_only=False, mode='min')
    checkpoint2 = keras.callbacks.ModelCheckpoint("models/"+config.model_type+"_up"+str(config.upscale)+"_jnoise"+str(config.jpeg_noise)+"_blur"+str(config.blur)+"_check_loss.h5", monitor='loss', verbose=0, save_best_only=True,
                                 save_weights_only=False, mode='min')
    callbacks_list = [checkpoint1,checkpoint2]

    for e,lr in zip(config.nepochs,config.learn_rates):
        K.set_value(model.optimizer.lr, lr)
        history_callback = model.fit(data_train, label_train, batch_size=config.batch_size, validation_split=0.25,
                               callbacks=callbacks_list, shuffle=True, epochs=e, verbose=1)
        model.load_weights("models/"+config.model_type+"_up"+str(config.upscale)+"_jnoise"+str(config.jpeg_noise)+"_blur"+str(config.blur)+"_check_val_loss.h5")
    pandas.DataFrame(history_callback.history).to_csv("models/"+config.model_type+"_up"+str(config.upscale)+"_blur"+str(config.blur)+"_history.csv")
    model.save_weights("models/"+config.model_type+"_up"+str(config.upscale)+"_jnoise"+str(config.jpeg_noise)+"_blur"+str(config.blur)+"_final.h5")


def predict_blocks(img,model,config):
    pre = []
    ps = config.patch_size
    up = config.upscale
    bl = config.process_size
    ld1 = int(math.ceil(img.shape[0]/float(bl)))
    ld2 = int(math.ceil(img.shape[1]/float(bl)))
    for c in range(img.shape[2]):
        print("Predict channel {0}".format(c+1))
        pre0 = numpy.zeros((img.shape[0]*up,img.shape[1]*up))
	
        for i in tqdm(range(ld1)):
            for j in range(ld2):
                predict = model.predict(img[i*bl:(i+1)*bl,j*bl:(j+1)*bl,c][numpy.newaxis,:,:,numpy.newaxis], batch_size=1)[0,:,:,0]
                pre0[i*bl*up:(i+1)*bl*up,j*bl*up:(j+1)*bl*up] = predict
	# Fixing edges
        for i in tqdm(range(ld1-1)):
            predict = model.predict(img[(i+1)*bl-ps:(i+1)*bl+ps,:,c][numpy.newaxis,:,:,numpy.newaxis], batch_size=1)[0,:,:,0]
            pre0[((i+1)*bl-ps)*up+1*ps:((i+1)*bl+ps)*up-1*ps,:] = predict[1*ps:-1*ps,:]
        for j in tqdm(range(ld2-1)):
            predict = model.predict(img[:,(j+1)*bl-ps:(j+1)*bl+ps,c][numpy.newaxis,:,:,numpy.newaxis], batch_size=1)[0,:,:,0]
            pre0[:,((j+1)*bl-ps)*up+1*ps:((j+1)*bl+ps)*up-1*ps] = predict[:,1*ps:-1*ps]

        pre.append(pre0)
    pre = numpy.squeeze(numpy.stack(pre,axis=2))
    pre[pre>1.0] = 1.0
    pre[pre<0.0] = 0.0
    return pre


def predict(config_file):
    print("Computing upscaled image")
    config = config_obj()
    config.parse(config_file)

    output_neural = config.input_image.split('.')[-2]+'_'+str(config.upscale)+'up_neural.jpg'
    output_bicubic = config.input_image.split('.')[-2]+'_'+str(config.upscale)+'up_bicubic.jpg'

    img = skimage.io.imread(config.input_image)
    img = img/255.

    possibles = globals().copy()
    possibles.update(locals())
    mymodel = possibles.get("model_"+config.model_type)
    model = mymodel(config)

    model.load_weights("models/"+config.model_type+"_up"+str(config.upscale)+"_jnoise"+str(config.jpeg_noise)+"_blur"+str(config.blur)+"_check_val_loss.h5")

    print('Predicting')
    img_neural = predict_blocks(img,model,config)
    img_bicubic = skimage.transform.resize(img,(config.upscale*img.shape[0],config.upscale*img.shape[1]), mode='constant')

    skimage.io.imsave(output_neural, img_neural, quality=100)
    skimage.io.imsave(output_bicubic, img_bicubic, quality=100)

def psnr(config_file):
    print("Computing PSNR")
    config = config_obj()
    config.parse(config_file)

    output_neural = config.input_image.split('.')[-2]+'_psnr_'+str(config.upscale)+'up_neural.jpg'
    output_bicubic = config.input_image.split('.')[-2]+'_psnr_'+str(config.upscale)+'up_bicubic.jpg'
    output_truth = config.input_image.split('.')[-2]+'_psnr_'+str(config.upscale)+'up_truth.jpg'
    output_down = config.input_image.split('.')[-2]+'_psnr_'+str(config.upscale)+'down.jpg'

    # Read input image
    img = skimage.io.imread(config.input_image)
    img = img/255.
    while(img.shape[0]%config.upscale!=0):
        img = skimage.transform.resize(img, (img.shape[0]-1, img.shape[1]), mode='constant')
        print("Resizing x to ",img.shape[0])
    while(img.shape[1]%config.upscale!=0):
        img = skimage.transform.resize(img, (img.shape[0], img.shape[1]-1), mode='constant')
        print("Resizing y to ",img.shape[1])
    shape = img.shape
    # Downscale the image for PSNR computation
    img_down = skimage.transform.resize(img, (int(shape[0] / config.upscale), int(shape[1] / config.upscale)), mode='constant')

    possibles = globals().copy()
    possibles.update(locals())
    mymodel = possibles.get("model_"+config.model_type)
    model = mymodel(config)

    # Load weights
    model.load_weights("models/"+config.model_type+"_up"+str(config.upscale)+"_jnoise"+str(config.jpeg_noise)+"_blur"+str(config.blur)+"_check_val_loss.h5")

    img_bicubic = skimage.transform.resize(img_down,(config.upscale*img_down.shape[0],config.upscale*img_down.shape[1]), mode='constant')
    img_neural = predict_blocks(img_down,model,config)
    
    skimage.io.imsave(output_neural,img_neural,quality=100)
    skimage.io.imsave(output_bicubic,img_bicubic,quality=100)
    skimage.io.imsave(output_truth,img,quality=100)
    skimage.io.imsave(output_down,img_down,quality=100)
    # PSNR computation:
    psnr_bicubic = []
    psnr_neural = []
    print("_"*100)
    for c in range(3):
        psnr_bicubic.append(skimage.measure.compare_psnr(img[:,:,c],img_bicubic[:,:,c]))
        psnr_neural.append(skimage.measure.compare_psnr(img[:,:,c],img_neural[:,:,c]))
        print("Channel {0}".format(c+1))
        print("Bicubic = {0:.3f} dB".format(psnr_bicubic[-1]))
        print("Neural  = {0:.3f} dB".format(psnr_neural[-1]))
        print("_"*100)
    print("Mean")
    print("Bicubic = {0:.3f} dB".format(numpy.mean(psnr_bicubic)))
    print("Neural  = {0:.3f} dB".format(numpy.mean(psnr_neural)))
